
const RandomWord = require("../lib/random_word");

describe("The RandomWord module is able to", () => {
  test("Read a file and return an array with an item for each line", () => {
    expect(RandomWord.readWordsFile(RandomWord.WORD_FILE)).toHaveLength(9);
  });

  test("get a random word that is never null", () => {
    const words = ["bagagem", "adjudicar", "ribeira"];
    let temp_word = RandomWord.getRandomWordFromFile(words)
    expect(words).toContain(temp_word);
  });
});

console.log(RandomWord)