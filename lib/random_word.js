const files = require("fs");
const path = require("path");

const WORD_FILE = path.resolve(__dirname,"../assets/words.txt");

const getRandomWord = () => {
  let words_array = readWordsFile(WORD_FILE);
  let word = getRandomWordFromFile(words_array).trim();
  return word;
};

const getRandomWordFromFile = words_array => {
    index = Math.floor((Math.random() * words_array.length));
    return words_array[index];
};

const readWordsFile = filename => {
  const words_array = files.readFileSync(filename, "utf-8").split("\n");
  var filtered = words_array.filter(function (el) {
    return el != '';
  });
  return filtered;
};

const Size =  words_array => {
    return words_array.length;
}

module.exports = {
  WORD_FILE,
  readWordsFile,
  getRandomWordFromFile,
  getRandomWord,
  Size
};

getRandomWord()
